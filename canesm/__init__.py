from canesm.canesm_ensemble import CanESMensemble
from canesm.canesm_setup import CanESMsetup
from canesm.job_submitter import CanESMsubmitter
from canesm.canesm_database import CanESMensembleDB

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions

from canesm.scripts.setup_ensemble import setup_ensemble_class