import click
import subprocess
import os
import canesm


@click.command()
@click.argument('config_file')
@click.version_option(version=canesm.__version__, message='%(version)s')
def monitor_ensemble(config_file):
    """
    Monitor an ensemble run using setup parameters determined from CONFIG_FILE
    """

    monitor_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'ensemble_monitor')
    subprocess.run('bokeh serve ' + monitor_file + ' --args ' + str(os.path.abspath(config_file)), shell=True)
