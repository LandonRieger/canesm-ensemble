from canesm import CanESMensemble
from canesm.util import RemoteFile, convert_date, add_time
from fabric import Connection
import canesm
import click
import yaml
import os
import logging
from typing import Union
import warnings


@click.command()
@click.option('--submit_ensemble', is_flag=True, show_default=True,
              default=False, help='DEPRECATED, use --submit')
@click.option('--submit', is_flag=True, show_default=True, default=False,
              help='start the simulations running after setup')
@click.option('--extend', default=None, type=int,
              help='Extend the ensemble by n years. If an integer the simulation is extended by years, if a string of'
                   'the format YYYY_mMM then the simulation is extended by YYYY years and MM months')
@click.version_option(version=canesm.__version__, message='%(version)s')
@click.argument('config_file')
def setup_ensemble(config_file: str, submit_ensemble: bool, extend: Union[int, str], submit: bool):
    """
    Setup an ensemble run with the parameters in CONFIG_FILE
    """

    if submit_ensemble:
        warnings.warn('--submit_ensemble will be deprecated, use "--submit"', DeprecationWarning)

    submit = submit or submit_ensemble

    ens = setup_ensemble_class(config_file)
    if submit:
        ens.submit_ensemble = submit

    if extend:
        ens.extend_ensemble(extend)
        save_extended_files(ens, config_file)
    else:
        ens.setup_ensemble()
        save_config_copy(ens, config_file)
        save_log_copy(ens)


def save_config_copy(ens: CanESMensemble, config_file: str):

    # move copies of the setup and log files over to the remote machine for safe keeping
    opt = yaml.load(open(config_file, 'r'), Loader=yaml.SafeLoader)
    with Connection(ens.machine, user=ens.user, gateway=Connection(ens.gateway_conn, user=ens.user)) as c:
        for idx, directory in enumerate(set(ens.run_directory)):
            if idx == 0:
                remote_yaml = os.path.join(directory, str(os.path.basename(config_file).split('.')[0]) + '-copy.yaml')
                c.put(os.path.realpath(config_file), remote_yaml)
                with RemoteFile(remote_yaml, ens.machine, ens.user, ens.gateway_conn, mode='a+') as f:
                    f.write('\n# setup by canesm-ensemble ' + canesm.__version__)
                c.run('chmod u=rw,g=r,o=r ' + remote_yaml)
                if 'config_table' in opt.keys():
                    table = table_path(opt['config_table'], config_file)
                    remote_table = os.path.join(directory, str(os.path.basename(table).split('.')[0]) + '-copy.txt')
                    c.put(table, remote_table)
                    c.run('chmod u=rw,g=r,o=r ' + remote_table)
            else:
                c.run(f'ln {remote_yaml} {os.path.join(directory, os.path.basename(remote_yaml))}')
                if 'config_table' in opt.keys():
                    c.run(f'ln {remote_table} {os.path.join(directory, os.path.basename(remote_table))}')


def save_log_copy(ens: CanESMensemble):

    # move copies of the setup and log files over to the remote machine for safe keeping
    with Connection(ens.machine, user=ens.user, gateway=Connection(ens.gateway_conn, user=ens.user)) as c:
        try:
            c.put(ens.log_file, os.path.join(ens.run_directory[0], str(os.path.basename(ens.log_file))))
        except Exception as e:
            logging.warning('error sending logfile to remote: ' + str(e))


def table_path(table: str, config_file: str) -> str:
    """
    Get the path to the table file. First the cwd is searched and if the file is not found the path relative to the
    configuration file is tested.

    Parameters
    ----------
    table:

    config_file:

    Returns
    -------
        Path to the table file
    """
    if os.path.isfile(table):
        table = table
    else:
        table = os.path.join(os.path.dirname(os.path.abspath(config_file)), table)
    return table


def read_table(filename: str):

    """
    Load the text table into a dictionary for parsing by the ensemble code

    Parameters
    ----------
    filename :
        path of the run configuration table
    """
    import pandas as pd
    data = pd.read_csv(filename, delim_whitespace=True, comment='#')

    data_dict = {}
    for key in data.keys():
        if ':' in key:
            try:
                data_dict[key.split(':')[0]][key.split(':')[1]] = [x for x in data[key].values]
            except KeyError:
                data_dict[key.split(':')[0]] = {}
                data_dict[key.split(':')[0]][key.split(':')[1]] = [x for x in data[key].values]
        else:
            data_dict[key] = [x for x in data[key].values]  # convert to list from array

    return data_dict


def write_table(data: dict, filename: str):

    import pandas as pd

    table_dict = {}
    for key in data.keys():

        # flatten the dictionary
        if type(data[key]) is dict:
            for dkey in data[key].keys():
                table_dict[f'{key}:{dkey}'] = data[key][dkey]
        else:
            table_dict[key] = data[key]

    pd.DataFrame(data=table_dict).to_csv(filename, sep='\t', index=False)


def save_extended_files(ens, config_file):

    # update the configuration and table files with new start/stop times
    opt = yaml.load(open(config_file, 'r'), Loader=yaml.SafeLoader)

    write_start = False
    write_stop = False
    if 'config_table' in opt.keys():
        table = table_path(opt['config_table'], config_file)
        table_opts = read_table(table)
        if 'start_time' in table_opts.keys():
            table_opts['start_time'] = ens.start_time
            write_start = True
        if 'stop_time' in table_opts.keys():
            table_opts['stop_time'] = ens.stop_time
            write_stop = True
        if write_start or write_stop:
            new_table = extended_filename(table)
            opt['config_table'] = os.path.basename(new_table)
            write_table(table_opts, new_table)

    if not write_start:
        opt['start_time'] = ens.start_time
    if not write_stop:
        opt['stop_time'] = ens.stop_time
    new_file = extended_filename(config_file)
    yaml.dump(opt, open(new_file, 'w'))

    save_config_copy(ens, new_file)


def extended_filename(old_file: str) -> str:

    base_file = ''.join(old_file.split('.')[:-1])
    file_extension = old_file.split('.')[-1]
    return base_file + '_extended.' + file_extension


def setup_ensemble_class(config_file: str) -> CanESMensemble:
    """
    Setup a CanESMensemble class using a YAML or JSON configuration file.

    Parameters
    ----------
    config_file
        path to file that will be used for setup

    Returns
    -------
        The setup ensemble class
    """
    opt = yaml.load(open(config_file, 'r'), Loader=yaml.SafeLoader)

    # setup options from table if available
    if 'config_table' in opt.keys():
        runs = read_table(table_path(opt['config_table'], config_file))
        opt['ensemble_size'] = len(runs['runid'])
        for key in runs.keys():
            if key in opt.keys():
                if type(opt[key]) is dict:
                    if set(opt[key]) & set(runs[key]):
                        logging.warning(key + ' values in yaml file will be overwritten by table values')
                    opt[key] = {**opt[key], **runs[key]}
                else:
                    logging.warning(key + ' values in yaml file will be overwritten by table values')
                    opt[key] = runs[key]
            else:
                opt[key] = runs[key]

    ens = CanESMensemble(ver=opt['ver'], config=opt['config'], runid=opt['runid'],
                         user=opt['user'], run_directory=opt['run_directory'], machine=opt['machine'])

    for var in opt.keys():
        setattr(ens, var, opt[var])

    # check if restart_every_x_years option is used and set dates accordingly
    if 'restart_dates' in opt.keys():
        if 'restart_every_x_years' in opt.keys():
            ens.restart_dates = [add_time(convert_date(opt['restart_dates']), i * opt['restart_every_x_years'])
                                 for i in range(opt['ensemble_size'])]
        else:
            ens.restart_dates = opt['restart_dates']

    if 'pp_rdm_num_pert' in opt.keys():
        if opt['pp_rdm_num_pert'] == 'from job number':
            ens.pp_rdm_num_pert = [i * 10 for i in range(0, ens.ensemble_size)]

    ens.broadcast_variables()
    ens.setup_database()
    return ens
