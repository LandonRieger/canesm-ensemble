import canesm
from canesm.scripts.setup_ensemble import setup_ensemble_class, save_extended_files
import click
from typing import Union
import warnings


@click.command()
@click.option('--submit_ensemble', is_flag=True, show_default=True,
              default=False, help='start the simulations running after setup')
@click.version_option(version=canesm.__version__, message='%(version)s')
@click.argument('config_file')
@click.argument('years')
def extend_ensemble(config_file: str, years: Union[int, str], submit_ensemble: bool = False):
    """
    Extend a completed ensemble by YEARS. This can be an integer or string of the format YYYY_mMM
    """

    warnings.warn('extend-ensemble will be deprecated, use "submit-ensemble --extend YEARS"', DeprecationWarning)

    # extend the ensemble
    ens = setup_ensemble_class(config_file)
    if submit_ensemble:
        ens.submit_ensemble = submit_ensemble
    ens.extend_ensemble(years)
    save_extended_files(ens, config_file)
