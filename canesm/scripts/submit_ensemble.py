import click
from canesm.scripts.setup_ensemble import setup_ensemble_class
from canesm.job_submitter import CanESMsubmitter
import canesm


@click.command()
@click.argument('config_file')
@click.option('-n', '--num_jobs', default=None, type=int,
              help='Only submit the next n jobs of the ensemble. If not provided all'
                   'jobs will be submitted')
@click.option('--show_status', is_flag=True, default=False,
              help='Show the current submission status of the ensemble and exit')
@click.option('--delay', default=0,
              help='Number of minutes to delay the submission of each job')
@click.version_option(version=canesm.__version__, message='%(version)s')
def submit_ensemble(config_file, num_jobs, show_status, delay):
    """
    Submit the next NUM_JOBS ensemble members. If NUM_JOBS is not provided all members
    will be submitted
    """

    ensemble = setup_ensemble_class(config_file)
    if show_status:
        ensemble.db.show_table()
        return
    else:
        for job in CanESMsubmitter(ensemble.db, delay=delay, num_jobs=num_jobs):
            job.submit()
