.. _help:

Troubleshooting
===============

Problems and Suggestions
************************

If you have suggestions for improvements or bug reports  you can submit them
`here <https://gitlab.science.gc.ca/CanESM/canesm-ensemble/issues>`_.

Help fixing bugs, adding features and improving the documentation is also very welcome. You can clone the code
from `gitlab <https://gitlab.science.gc.ca/CanESM/canesm-ensemble/issues>`_, add your changes, and
make a merge request.


Common Issues
*************

#. **I was running canesm-ensemble and now my terminal is frozen...**

   This can happen when ``ctrl+c`` is used to stop the program, try entering ``reset`` into the terminal
   window once or twice (even though you won't be able to see it), this usually fixes the problem.