from canesm.scripts.setup_ensemble import setup_ensemble, setup_ensemble_class
import os
from fabric import Connection
from click.testing import CliRunner
from canesm.util import RemoteDBConn
import pytest


setup_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'setup_files')


class ModelRun(object):

    def __init__(self, config_file):
        self.ens = setup_ensemble_class(os.path.join(setup_folder, config_file))

    def __enter__(self):
        return self.ens

    def __exit__(self, *args):
        self.ens.delete_ensemble()


class TestRemoteConfig:

    # @pytest.mark.skip(reason='scrd105 authentication error')
    def test_successful_setup(self):

        config_file = 'develop_canesm_multithread.yaml'
        config_file_copy = 'develop_canesm_multithread-copy.yaml'
        config = os.path.join(setup_folder, config_file)

        with ModelRun(config) as ens:
            runner = CliRunner()
            result = runner.invoke(setup_ensemble, [config])
            print(result.stdout)
            assert result.exit_code == 0

            # check that yaml file was copied properly
            with Connection(ens.machine, user=ens.user,
                            gateway=Connection(ens.gateway_conn, user=ens.user)) as c:
                with c.cd(ens.run_directory[0]):
                    output = c.run(f'test -e {config_file_copy} && echo file exists || echo file not found')

            assert output.stdout == 'file exists\n'

            # check that database was created
            with RemoteDBConn(ens.db.db_file, ens.machine, ens.user, ens.gateway_conn) as conn:
                c = conn.cursor()
                c.execute('''SELECT * FROM status''')
                test = c.fetchall()

            assert test == [('ens_tst-000', 0, 0, 'ens_tst-000_1990m01_2014m12_hare_job',
                             'canesm_ensemble/testing/ens_ci1/ens_tst-000', 0, 1),
                            ('ens_tst-001', 0, 1, 'ens_tst-001_1990m01_2014m12_hare_job',
                             'canesm_ensemble/testing/ens_ci2/ens_tst-001', 1, 1),
                            ('ens_tst-002', 0, 2, 'ens_tst-002_1990m01_2014m12_hare_job',
                             'canesm_ensemble/testing/ens_ci3/ens_tst-002', 2, 1)]
