from canesm.scripts.extend_ensemble import extend_ensemble
import pytest
import os
from click.testing import CliRunner


setup_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'setup_files')


@pytest.mark.skip('no finished simulation to test setup with at the moment')
class TestExtend:

    def test_extend(self):
        extend_ensemble([os.path.join(setup_folder, 'test_config_table_dates.yaml'), '10'])


if __name__ == '__main__':
    TestExtend().test_extend()
