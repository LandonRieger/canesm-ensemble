import canesm
from canesm.scripts.setup_ensemble import setup_ensemble
from canesm.scripts.submit_ensemble import submit_ensemble
from canesm.scripts.monitor_ensemble import monitor_ensemble
from canesm.scripts.extend_ensemble import extend_ensemble
from click.testing import CliRunner


class TestInstall:

    def test_setup_ensemble(self):

        runner = CliRunner()
        result = runner.invoke(setup_ensemble, ['--version'])
        assert result.stdout.strip() == canesm.__version__

    def test_submit_ensemble(self):

        runner = CliRunner()
        result = runner.invoke(submit_ensemble, ['--version'])
        assert result.stdout.strip() == canesm.__version__

    def test_monitor_ensemble(self):

        runner = CliRunner()
        result = runner.invoke(monitor_ensemble, ['--version'])
        assert result.stdout.strip() == canesm.__version__

    def test_extend_ensemble(self):

        runner = CliRunner()
        result = runner.invoke(extend_ensemble, ['--version'])
        assert result.stdout.strip() == canesm.__version__


if __name__ == "__main__":
    TestInstall().test_extend_ensemble()